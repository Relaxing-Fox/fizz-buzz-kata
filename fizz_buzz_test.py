import unittest as ut
from fizz_buzz import fizz_buzz


class FizzBuzzTest(ut.TestCase):

    def test_normal_numbers_return_same_number(self):
        self.assertEqual(fizz_buzz(1), 1)
        self.assertEqual(fizz_buzz(11), 11)
        self.assertEqual(fizz_buzz(337), 337)

    def test_multiples_of_3_return_Fizz(self):
        self.assertEqual(fizz_buzz(3), 'Fizz')
        self.assertEqual(fizz_buzz(9), 'Fizz')
        self.assertEqual(fizz_buzz(123), 'Fizz')

    def test_multiples_of_5_return_Buzz(self):
        self.assertEqual(fizz_buzz(5), 'Buzz')
        self.assertEqual(fizz_buzz(20), 'Buzz')
        self.assertEqual(fizz_buzz(200), 'Buzz')

    def test_multiples_of_3_and_5_return_FizzBuzz(self):
        self.assertEqual(fizz_buzz(15), 'Fizz Buzz')
        self.assertEqual(fizz_buzz(45), 'Fizz Buzz')
        self.assertEqual(fizz_buzz(135), 'Fizz Buzz')

    def test_multiples_of_7_return_pop(self):
        self.assertEqual(fizz_buzz(7), 'Pop')
        self.assertEqual(fizz_buzz(28), 'Pop')
        self.assertEqual(fizz_buzz(77), 'Pop')

    def test_multiples_of_3_and_7_return_FizzPop(self):
        self.assertEqual(fizz_buzz(21), 'Fizz Pop')
        self.assertEqual(fizz_buzz(63), 'Fizz Pop')
        self.assertEqual(fizz_buzz(126), 'Fizz Pop')

    def test_multiples_of_5_and_7_return_BuzzPop(self):
        self.assertEqual(fizz_buzz(35), 'Buzz Pop')
        self.assertEqual(fizz_buzz(70), 'Buzz Pop')
        self.assertEqual(fizz_buzz(140), 'Buzz Pop')

    def test_multiples_of_3_and_5_and_7_return_FizzBuzzPop(self):
        self.assertEqual(fizz_buzz(105), 'Fizz Buzz Pop')
        self.assertEqual(fizz_buzz(210), 'Fizz Buzz Pop')
        self.assertEqual(fizz_buzz(315), 'Fizz Buzz Pop')

    def test_multiples_of_2_return_fuzz(self):
        self.assertEqual(fizz_buzz(1), 1)
        self.assertEqual(fizz_buzz(2), 'Fuzz')
        self.assertEqual(fizz_buzz(16), 'Fuzz')

