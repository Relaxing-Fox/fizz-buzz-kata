
def fizz_buzz(num):

    # Finds numbers only divisible of 3, 5, and 7 in order to print out Fizz Buzz Pop.
    if num % 7 == 0 and num % 3 == 0 and num % 5 == 0:
        return 'Fizz Buzz Pop'
    # Finds numbers only divisible of 3 and 5, but not 7 in order to print out Fizz Buzz.
    if num % 5 == 0 and num % 3 == 0 and not num % 7 == 0:
        return 'Fizz Buzz'
    # Finds numbers only divisible of 5 and 7 to print out Buzz Pop.
    if num % 5 == 0 and num % 7 == 0:
        return 'Buzz Pop'
    # Finds numbers only divisible of 3 and 7 to print out Fizz Pop.
    if num % 3 == 0 and num % 7 == 0:
        return 'Fizz Pop'
    # Finds numbers only divisible of 3 to print out Fizz.
    if num % 3 == 0:
        return 'Fizz'
    # Finds numbers only divisible of 7 to print out Pop.
    if num % 7 == 0:
        return 'Pop'
    # Finds numbers only divisible of 5 to print out Buzz.
    if num % 5 == 0:
        return 'Buzz'
    # Finds numbers only divisible of 2 to print out Fuzz.
    if num % 2 == 0:
        return 'Fuzz'
    # Finds all other numbers.
    else:
        return num


